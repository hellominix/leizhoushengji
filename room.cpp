#include "room.h"
#include "seat.h"

Room::Room(QWidget *parent)
    : QGraphicsView(parent)
{
    setObjectName(QStringLiteral("room"));
    QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    sizePolicy.setHorizontalStretch(10);
    sizePolicy.setVerticalStretch(4);
    sizePolicy.setHeightForWidth(this->sizePolicy().hasHeightForWidth());
    setSizePolicy(sizePolicy);
    setMinimumSize(QSize(955, 745));
    scene = new QGraphicsScene(this);
    scene->setBackgroundBrush(Qt::black);
    populateScene();
    setScene(scene);
}

void Room::resizeEvent(QResizeEvent *event)
{
    scene->clear();
    populateScene();
}

void Room::populateScene()
{
    QImage image(":/100.bmp");
    int nseat=0;
    for (int j = image.height()/2 ;   ; j += 1.5*image.height())
    {

        for (int i = image.width()/2; i < width() - image.width()/2; i += 1.5*image.width())
        {

            QGraphicsItem *item = new Seat();
            item->setPos(QPointF(i,j));
            scene->addItem(item);
            nseat++;
        }
        if(nseat > 100)
            break;
    }
}

