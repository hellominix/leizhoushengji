#-------------------------------------------------
#
# Project created by QtCreator 2013-04-06T11:38:36
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = leizhoushengji
TEMPLATE = app


SOURCES += main.cpp \
    widget.cpp \
    seat.cpp \
    room.cpp

HEADERS  += \
    widget.h \
    seat.h \
    room.h

FORMS    +=

RESOURCES += \
    images.qrc
