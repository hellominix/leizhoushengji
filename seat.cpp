
#include "seat.h"
#include <QDebug>
#include <QtWidgets>

Seat::Seat()
{
    leftseatpress = false;
    downseatpress = false;
    upseatpress = false;
    rightseatpress = false;
    setFlags(ItemIsSelectable);
}

QRectF Seat::boundingRect() const
{
    return QRectF(0,0,QPixmap(":100.bmp").width(),QPixmap(":100.bmp").height());
}

void Seat::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);
    QImage image(":le.bmp");
    QImage image2 = image.convertToFormat(QImage::Format_ARGB32_Premultiplied);
    qDebug() << image2.format();
    qDebug() <<image2.save("left2","JPG");
//    QImage image(":100.bmp");
//    QImage image2 = image.convertToFormat(QImage::Format_ARGB32_Premultiplied);
//    qDebug() << image2.format();
//    qDebug() <<image2.save("table","JPG");
    painter->drawPixmap(QPointF(0.0,0.0),QPixmap(":100.bmp"));
    if(leftseatpress)
    painter->drawPixmap(QPointF(-15.0,10.0),QPixmap(":leftSeat.jpg"));
    if(downseatpress)
    painter->drawPixmap(QPointF(30.0,-7.0),QPixmap(":downSeat.jpg"));
    if(upseatpress)
    painter->drawPixmap(QPointF(52.0,-19.0),QPixmap(":upSeat.jpg"));
    if(rightseatpress)
    painter->drawPixmap(QPointF(90.0,-11.0),QPixmap(":rightSeat.jpg"));
}
void Seat::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if( event->pos().x() > 0 && event->pos().x() < 40 )
       leftseatpress = true;
    if( event->pos().x() > 40 && event->pos().x() < 70 )
       downseatpress = true;
    if( event->pos().x() > 70 && event->pos().x() < 100 )
       upseatpress = true;
    if( event->pos().x() > 100 && event->pos().x() < 130 )
       rightseatpress = true;
    update();
    QGraphicsItem::mousePressEvent(event);
}

void Seat::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->modifiers() & Qt::ShiftModifier) {
        update();
        return;
    }
    QGraphicsItem::mouseMoveEvent(event);
}

void Seat::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);
    update();
}
