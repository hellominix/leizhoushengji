#ifndef ROOM_H
#define ROOM_H

#include <QGraphicsView>
#include <QGraphicsScene>

class Room: public QGraphicsView
{
    Q_OBJECT
public:
    Room(QWidget *parent = 0);
protected:
    void resizeEvent(QResizeEvent *event);

private:
    void populateScene();
    QGraphicsScene *scene;
};
#endif // ROOM_H
