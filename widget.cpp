#include "widget.h"
#include "seat.h"
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent)
{
    if (this->objectName().isEmpty())
        this->setObjectName(QStringLiteral("Widget"));
    this->resize(1241, 767);
    horizontalLayout_3 = new QHBoxLayout(this);
    horizontalLayout_3->setSpacing(6);
    horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
    horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
    horizontalLayout_2 = new QHBoxLayout();
    horizontalLayout_2->setSpacing(6);
    horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
    verticalLayout = new QVBoxLayout();
    verticalLayout->setSpacing(6);
    verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
    verticalLayout->setSizeConstraint(QLayout::SetMaximumSize);

    room = new Room(this);
    verticalLayout->addWidget(room);


    horizontalLayout_2->addLayout(verticalLayout);

    verticalLayout_2 = new QVBoxLayout();
    verticalLayout_2->setSpacing(6);
    verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
    verticalLayout_2->setSizeConstraint(QLayout::SetMinimumSize);
    horizontalLayout = new QHBoxLayout();
    horizontalLayout->setSpacing(6);
    horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
    horizontalLayout->setSizeConstraint(QLayout::SetMinimumSize);
    pushButton_2 = new QPushButton(this);
    pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
    QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
    sizePolicy1.setHorizontalStretch(1);
    sizePolicy1.setVerticalStretch(0);
    sizePolicy1.setHeightForWidth(pushButton_2->sizePolicy().hasHeightForWidth());
    pushButton_2->setSizePolicy(sizePolicy1);

    horizontalLayout->addWidget(pushButton_2);

    pushButton_3 = new QPushButton(this);
    pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
    sizePolicy1.setHeightForWidth(pushButton_3->sizePolicy().hasHeightForWidth());
    pushButton_3->setSizePolicy(sizePolicy1);

    horizontalLayout->addWidget(pushButton_3);

    pushButton = new QPushButton(this);
    pushButton->setObjectName(QStringLiteral("pushButton"));
    sizePolicy1.setHeightForWidth(pushButton->sizePolicy().hasHeightForWidth());
    pushButton->setSizePolicy(sizePolicy1);

    horizontalLayout->addWidget(pushButton);


    verticalLayout_2->addLayout(horizontalLayout);

    treeWidget = new QTreeWidget(this);
    QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem(treeWidget);
    new QTreeWidgetItem(__qtreewidgetitem);
    new QTreeWidgetItem(__qtreewidgetitem);
    new QTreeWidgetItem(__qtreewidgetitem);
    new QTreeWidgetItem(__qtreewidgetitem);
    new QTreeWidgetItem(__qtreewidgetitem);
    QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem(treeWidget);
    new QTreeWidgetItem(__qtreewidgetitem1);
    new QTreeWidgetItem(__qtreewidgetitem1);
    QTreeWidgetItem *__qtreewidgetitem2 = new QTreeWidgetItem(treeWidget);
    new QTreeWidgetItem(__qtreewidgetitem2);
    new QTreeWidgetItem(__qtreewidgetitem2);
    treeWidget->setObjectName(QStringLiteral("treeWidget"));
    sizePolicy1.setHeightForWidth(treeWidget->sizePolicy().hasHeightForWidth());
    treeWidget->setSizePolicy(sizePolicy1);

    verticalLayout_2->addWidget(treeWidget);

    tableWidget = new QTableWidget(this);
    if (tableWidget->columnCount() < 3)
        tableWidget->setColumnCount(3);
    QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
    tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
    QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
    tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
    QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
    tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
    tableWidget->setObjectName(QStringLiteral("tableWidget"));
    QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Expanding);
    sizePolicy2.setHorizontalStretch(1);
    sizePolicy2.setVerticalStretch(0);
    sizePolicy2.setHeightForWidth(tableWidget->sizePolicy().hasHeightForWidth());
    tableWidget->setSizePolicy(sizePolicy2);

    verticalLayout_2->addWidget(tableWidget);


    horizontalLayout_2->addLayout(verticalLayout_2);


    horizontalLayout_3->addLayout(horizontalLayout_2);


    retranslateUi(this);
    QMetaObject::connectSlotsByName(this);
}



void Widget::retranslateUi(QWidget *Widget)
{
    Widget->setWindowTitle(QApplication::translate("Widget", "Widget", 0));
    pushButton_2->setText(QApplication::translate("Widget", "\350\256\276\347\275\256", 0));
    pushButton_3->setText(QApplication::translate("Widget", "\346\237\245\346\211\276", 0));
    pushButton->setText(QApplication::translate("Widget", "\345\245\275\345\217\213", 0));
    QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
    ___qtreewidgetitem->setText(0, QApplication::translate("Widget", "\346\210\277\351\227\264", 0));

    const bool __sortingEnabled = treeWidget->isSortingEnabled();
    treeWidget->setSortingEnabled(false);
    QTreeWidgetItem *___qtreewidgetitem1 = treeWidget->topLevelItem(0);
    ___qtreewidgetitem1->setText(0, QApplication::translate("Widget", "\345\220\214\345\237\2161\345\214\272", 0));
    QTreeWidgetItem *___qtreewidgetitem2 = ___qtreewidgetitem1->child(0);
    ___qtreewidgetitem2->setText(0, QApplication::translate("Widget", "\351\233\267\347\245\226\347\245\240", 0));
    QTreeWidgetItem *___qtreewidgetitem3 = ___qtreewidgetitem1->child(1);
    ___qtreewidgetitem3->setText(0, QApplication::translate("Widget", "\344\270\211\345\205\203\345\241\224", 0));
    QTreeWidgetItem *___qtreewidgetitem4 = ___qtreewidgetitem1->child(2);
    ___qtreewidgetitem4->setText(0, QApplication::translate("Widget", "\350\245\277\346\271\226\345\205\254\345\233\255", 0));
    QTreeWidgetItem *___qtreewidgetitem5 = ___qtreewidgetitem1->child(3);
    ___qtreewidgetitem5->setText(0, QApplication::translate("Widget", "\344\270\211\345\205\203\345\241\224", 0));
    QTreeWidgetItem *___qtreewidgetitem6 = ___qtreewidgetitem1->child(4);
    ___qtreewidgetitem6->setText(0, QApplication::translate("Widget", "\345\244\251\345\256\201\345\257\272", 0));
    QTreeWidgetItem *___qtreewidgetitem7 = treeWidget->topLevelItem(1);
    ___qtreewidgetitem7->setText(0, QApplication::translate("Widget", "\345\220\214\345\237\2162\345\214\272", 0));
    QTreeWidgetItem *___qtreewidgetitem8 = ___qtreewidgetitem7->child(0);
    ___qtreewidgetitem8->setText(0, QApplication::translate("Widget", "\351\233\267\347\245\226\347\245\240", 0));
    QTreeWidgetItem *___qtreewidgetitem9 = ___qtreewidgetitem7->child(1);
    ___qtreewidgetitem9->setText(0, QApplication::translate("Widget", "\344\270\211\345\205\203\345\241\224", 0));
    QTreeWidgetItem *___qtreewidgetitem10 = treeWidget->topLevelItem(2);
    ___qtreewidgetitem10->setText(0, QApplication::translate("Widget", "\345\220\214\345\237\2163\345\214\272", 0));
    QTreeWidgetItem *___qtreewidgetitem11 = ___qtreewidgetitem10->child(0);
    ___qtreewidgetitem11->setText(0, QApplication::translate("Widget", "\351\233\267\347\245\226\347\245\240", 0));
    QTreeWidgetItem *___qtreewidgetitem12 = ___qtreewidgetitem10->child(1);
    ___qtreewidgetitem12->setText(0, QApplication::translate("Widget", "\344\270\211\345\205\203\345\241\224", 0));
    treeWidget->setSortingEnabled(__sortingEnabled);

    QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
    ___qtablewidgetitem->setText(QApplication::translate("Widget", "\347\224\250\346\210\267\345\220\215", 0));
    QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
    ___qtablewidgetitem1->setText(QApplication::translate("Widget", "\347\212\266\346\200\201", 0));
    QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
    ___qtablewidgetitem2->setText(QApplication::translate("Widget", "\347\247\257\345\210\206", 0));
}



Widget::~Widget()
{

}
