#ifndef SEAT_H
#define SEAT_H
#include <QColor>
#include <QGraphicsItem>

class Seat : public QGraphicsItem
{
public:
    Seat();

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
private:
    bool leftseatpress;
    bool downseatpress;
    bool upseatpress;
    bool rightseatpress;
};
#endif // SEAT_H
